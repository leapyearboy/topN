#!/usr/bin/ruby
# small file to assist in generating big lists of files
require 'yaml'

def write_numbers(file,opt,rand)
  i = 0
  n = opt['number_of_lines']
  until i>=n
    i += 1
    file.puts(rand(opt['range_start']...opt['range_end']))
  end
end 

# Build a file
begin
  options = YAML.load_file('create_sample_file.yaml')
  r = Random.new
  file = File.open('data_file','w')
  write_numbers(file,options,r)
rescue IOError => e
  puts "Error: Hope you didn't run out of disk space..."
ensure 
  file.close()
end