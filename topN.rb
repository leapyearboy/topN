#!/usr/bin/ruby
require 'yaml'

begin

  	options = YAML.load_file('topN_config.yaml')		# Load the config, maybe OTT but I <3 yaml based config
	topN = Array.new(options['number_of_results'], 0)	# Create an 0 value array with length N
  	
	input_file = File.foreach('data_file') do |line| 	# Open the file and iterate on each line

		line_value = line.split.map(&:to_i)[0]			# Parse the line, remove newline chars and force to integer
		current_min = topN.each_with_index.min 			# Figure out where our minimum value is, assign variables to make loop code look prettier
		current_min_value = current_min[0]
		current_min_index = current_min[1]

		if line_value > current_min_value
			topN[current_min_index] = line_value		# Overwrite the minimum value with the new value
			if (options['debug_mode'])
				puts "overwrote index: #{current_min_index},"\
					 " value: #{current_min_value} with #{line_value}"
				sleep(1)								# As the array is N * 0 initially this will run N times at start, was good for testing
			end
		end
	end

	topN.sort.reverse									# Sorting in reverse mode on a significantly smaller dataset
	output_file = File.open('sorted_data','w')
	output_file.puts(topN)								# Write to the output file
	puts 'The following numbers were the highest in the datafile:'
	puts topN
end